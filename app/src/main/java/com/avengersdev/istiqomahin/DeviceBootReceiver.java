package com.avengersdev.istiqomahin;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.avengersdev.istiqomahin.activity.SettingActivity;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by zap on 12/17/2017.
 */

public class DeviceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            /* Setting the alarm here */
            Notification.Builder builder = new Notification.Builder(context);
            builder.setContentTitle("Selamat Malan!");
            builder.setContentText("Isi capaian ibadahnya ya:)");
            builder.setSmallIcon(R.drawable.ic_launcher_background);
            Intent notificationIntent = new Intent(context, NotificationPublisher.class);
            notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
            notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, builder.build());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 22);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY , pendingIntent);
            Toast.makeText(context, "Notifikasi aktif", Toast.LENGTH_SHORT).show();
        }
    }

}
