package com.avengersdev.istiqomahin.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.helper.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class NotifActivity extends AppCompatActivity {

    private PrefManager prefManager;
    private Long timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefManager = new PrefManager(getApplicationContext());
        applyTheme(prefManager.getTema());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_notif);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Notifikasi");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        refreshTime();
    }

    private void refreshTime() {
        TextView tv1 = findViewById(R.id.set1);
        TextView tv2 = findViewById(R.id.set2);
        TextView tv3 = findViewById(R.id.set3);
        TextView tv4 = findViewById(R.id.set4);
        TextView tv5 = findViewById(R.id.set5);
        tv1.setText(prefManager.getSubh());
        tv2.setText(prefManager.getDuhr());
        tv3.setText(prefManager.getAshr());
        tv4.setText(prefManager.getMgrb());
        tv5.setText(prefManager.getIsha());
    }



    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }
}
