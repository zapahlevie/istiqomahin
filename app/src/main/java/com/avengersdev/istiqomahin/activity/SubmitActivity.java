package com.avengersdev.istiqomahin.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.adapter.SubmitAdapter;
import com.avengersdev.istiqomahin.controller.AppController;
import com.avengersdev.istiqomahin.helper.DBHelper;
import com.avengersdev.istiqomahin.helper.PrefManager;
import com.avengersdev.istiqomahin.model.Rapor;
import com.avengersdev.istiqomahin.server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class SubmitActivity extends AppCompatActivity {
    private DBHelper db;
    private PrefManager prefManager;
    private ProgressDialog pDialog;
    private String user_id;
    private String name;
    private ArrayList<Rapor> daftarIbadah = new ArrayList<Rapor>();
    private RequestQueue rq;
    private ListView list;
    private SubmitAdapter adapter;
    private Rapor stock;
    private List<Rapor> stockList = new ArrayList<Rapor>();
    private String currentDateandTime;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefManager = new PrefManager(getApplicationContext());
        applyTheme(prefManager.getTema());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_submit);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Submit : "+new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        db = new DBHelper(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        user_id = user.get("uid");
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        list = (ListView) findViewById(R.id.list);
        adapter = new SubmitAdapter(SubmitActivity.this, daftarIbadah);
        list.setAdapter(adapter);
        String url = Server.UrlIbadah+"select.php?user_id="+user_id;
        JsonArrayRequest jArr = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(SubmitActivity.class.getSimpleName(), response.toString());
                        hidePDialog();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Rapor map = new Rapor();
                                map.setId(obj.getString("id"));
                                map.setNamaIbadah(obj.getString("nama"));
                                map.setTercapai("");
                                map.setTarget(obj.getString("target"));
                                map.setKategori(obj.getString("kategori"));
                                map.setStatus("");
                                map.setTanggal("");
                                map.setTipe(obj.getString("tipe"));
                                map.setSatuan(obj.getString("satuan"));
                                map.setSelected(false);
                                daftarIbadah.add(map);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(SubmitActivity.class.getSimpleName(), "Error: " + error.getMessage());
                hidePDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getDefault());
        currentDateandTime = sdf.format(new Date());
        rq = Volley.newRequestQueue(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_save, menu);
        Drawable drawable = menu.getItem(0).getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        }
        if(prefManager.isSubmitted(currentDateandTime)){
            assert drawable != null;
            drawable.setAlpha(200);
        }
        else{
            drawable.setAlpha(255);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_save:
                if(prefManager.isSubmitted(currentDateandTime)){
                    Toast.makeText(SubmitActivity.this, "Laporan hari ini sudah disubmit", Toast.LENGTH_SHORT).show();;
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(SubmitActivity.this);
                    builder.setMessage("Submit?")
                            .setTitle("Konfirmasi")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    int count = list.getCount();
                                    int i = 0;
                                    for (i = 0; i < count; i++) {
                                        View childView = getViewByPosition(i, list);
                                        TextView nama = (TextView) childView.findViewById(R.id.nama);
                                        EditText tercapai_isi = (EditText) childView.findViewById(R.id.tercapai_isi);
                                        TextView target = (TextView) childView.findViewById(R.id.target);
                                        TextView kategori = (TextView) childView.findViewById(R.id.kategori);
                                        CheckBox tercapai_cek = (CheckBox) childView.findViewById(R.id.tercapai_cek);
                                        String namaIbadah = nama.getText().toString();
                                        String tercapaiIbadah = tercapai_isi.getText().toString();
                                        String targetIbadah = target.getText().toString();
                                        String kategoriIbadah = kategori.getText().toString();
                                        if (tercapai_cek.isChecked()) {
                                            tercapaiIbadah = "1";
                                        }
                                        if(tercapaiIbadah.isEmpty()){
                                            tercapaiIbadah = "0";
                                        }
                                        stock = new Rapor();
                                        stock.setNamaIbadah(namaIbadah);
                                        stock.setTercapai(tercapaiIbadah);
                                        stock.setTarget(targetIbadah);
                                        stock.setKategori(kategoriIbadah);
                                        if (Integer.valueOf(tercapaiIbadah) >= Integer.valueOf(targetIbadah)) {
                                            stock.setStatus("Tuntas");
                                        } else {
                                            stock.setStatus("Belum Tuntas");
                                        }
                                        stock.setTanggal(currentDateandTime);
                                        stockList.add(stock);
                                        simpanData(stockList.get(i));
                                    }
                                    Toast.makeText(SubmitActivity.this, "Data berhasil disimpan!", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                    prefManager.setSubmitted(currentDateandTime);
                                    onBackPressed();
                                }
                            })
                            .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    // CANCEL
                                }
                            });
                    // Create the AlertDialog object and return it
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                return(true);
        }
        return(super.onOptionsItemSelected(item));
    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    public void simpanData(final Rapor r){
        String url = Server.UrlRapor+"insert.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        //Toast.makeText(InputActivity.this, response, Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.getMessage());
                        //Toast.makeText(InputActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("nama_ibadah", r.getNamaIbadah());
                params.put("tercapai", r.getTercapai());
                params.put("target", r.getTarget());
                params.put("kategori", r.getKategori());
                params.put("status", r.getStatus());
                params.put("tanggal", r.getTanggal());
                params.put("user_id", user_id);
                return params;
            }
        };
        rq.add(postRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }
}