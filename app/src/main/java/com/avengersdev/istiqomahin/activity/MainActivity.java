package com.avengersdev.istiqomahin.activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.avengersdev.istiqomahin.NotificationPublisher;
import com.avengersdev.istiqomahin.helper.PrefManager;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.helper.DBHelper;
import com.avengersdev.istiqomahin.server.Server;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private PrefManager prefManager;
    private DBHelper db;
    private String user_id;
    private String name;
    private RequestQueue rq;
    private Button btn_submit, btn_target, btn_berita, btn_rapor, btn_grup, btn_setting;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefManager = new PrefManager(getApplicationContext());
        applyTheme(prefManager.getTema());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DBHelper(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        user_id = user.get("uid");
        name = user.get("name");
        TextView username = (TextView)findViewById(R.id.user_name);
        username.setText(name);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btn_submit = (Button)findViewById(R.id.btn_submit);
        btn_target = (Button)findViewById(R.id.btn_target);
        btn_berita = (Button)findViewById(R.id.btn_berita);
        btn_rapor = (Button)findViewById(R.id.btn_rapor);
        btn_grup = (Button)findViewById(R.id.btn_grup);
        btn_setting = (Button)findViewById(R.id.btn_setting);

        View submit = (View)findViewById((R.id.cv_submit));
        View target = (View)findViewById((R.id.cv_target));
        View berita = (View)findViewById((R.id.cv_berita));
        View rapor = (View)findViewById((R.id.cv_rapor));
        View grup = (View)findViewById((R.id.cv_grup));
        View setting = (View)findViewById((R.id.cv_setting));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(SubmitActivity.class);
            }
        });
        target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(IbadahActivity.class);
            }
        });
        berita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(BeritaActivity.class);
            }
        });
        rapor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(RaporActivity.class);
            }
        });
        grup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(GrupActivity.class);
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(SettingActivity.class);
            }
        });
        CustomizeMenu();
        loadTime();
        String[] subh = prefManager.getSubh().split(":");
        String[] duhr = prefManager.getDuhr().split(":");
        String[] ashr = prefManager.getAshr().split(":");
        String[] mgrb = prefManager.getMgrb().split(":");
        String[] isha = prefManager.getIsha().split(":");
        prayer(2, "Waktu Shalat Shubuh!", Integer.valueOf(subh[0]), Integer.valueOf(subh[1]));
        prayer(3, "Waktu Shalat Dhuhur!", Integer.valueOf(duhr[0]), Integer.valueOf(duhr[1]));
        prayer(4, "Waktu Shalat Ashar!", Integer.valueOf(ashr[0]), Integer.valueOf(ashr[1]));
        prayer(5, "Waktu Shalat Maghrib!", Integer.valueOf(mgrb[0]), Integer.valueOf(mgrb[1]));
        prayer(6, "Waktu Shalat Isya!", Integer.valueOf(isha[0]), Integer.valueOf(isha[1]));

        CircularImageView circularImageView = findViewById(R.id.photo);
        Picasso.with(getApplicationContext())
                .load(Server.UrlPhoto+user_id+".jpg")
                .error(R.drawable.main_profile)
                .into(circularImageView);
    }

    private void prayer(int id, String text, int hour, int minute) {
        Intent notificationIntent = new Intent(getApplicationContext(), NotificationPublisher.class);
        boolean alarmUp = (PendingIntent.getBroadcast(getApplicationContext(), id, notificationIntent,
                PendingIntent.FLAG_NO_CREATE) != null);
        if(!alarmUp){
            Notification.Builder builder = new Notification.Builder(getApplicationContext());
            builder.setContentTitle(text);
            builder.setContentText("Segerakan Sholat ya:)");
            builder.setSmallIcon(R.drawable.ic_launcher_background);
            builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
            builder.setLights(Color.BLUE, 3000, 3000);
            //Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri alarmSound = Uri.parse("android.resource://"+getPackageName()+"/raw/sound");
            builder.setSound(alarmSound);
            notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);
            notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, builder.build());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY , pendingIntent);
        }
        //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);
    }

    private void loadTime() {
        rq = Volley.newRequestQueue(getApplicationContext());
        Long timestamp = System.currentTimeMillis() / 1000;
        final String url = "http://api.aladhan.com/timings/"+timestamp+"?latitude=-7.2574719&longitude=112.7520883&method=11";
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                            JSONObject timings = data.getJSONObject("timings");
                            String subh = timings.getString("Fajr");
                            String duhr = timings.getString("Dhuhr");
                            String ashr = timings.getString("Asr");
                            String mgrb = timings.getString("Maghrib");
                            String isha = timings.getString("Isha");
                            prefManager.setSubh(subh);
                            prefManager.setDuhr(duhr);
                            prefManager.setAshr(ashr);
                            prefManager.setMgrb(mgrb);
                            prefManager.setIsha(isha);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("Response", response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );
        rq.add(getRequest);
    }

    private void CustomizeMenu() {
        if(prefManager.getTema().equals("theme6")){
            btn_submit.setTextColor(fetchColor());
            btn_target.setTextColor(fetchColor());
            btn_berita.setTextColor(fetchColor());
            btn_rapor.setTextColor(fetchColor());
            btn_grup.setTextColor(fetchColor());
            btn_setting.setTextColor(fetchColor());
            setDrawable(btn_submit, R.drawable.ic_event_available_black_24dp);
            setDrawable(btn_target, R.drawable.ic_event_note_black_24dp);
            setDrawable(btn_berita, R.drawable.ic_description_black_24dp);
            setDrawable(btn_rapor, R.drawable.ic_timeline_black_24dp);
            setDrawable(btn_grup, R.drawable.ic_people_black_24dp);
            setDrawable(btn_setting, R.drawable.ic_settings_black_24dp);
        }
        else{
            btn_submit.setTextColor(fetchColorPrimary());
            btn_target.setTextColor(fetchColorPrimary());
            btn_berita.setTextColor(fetchColorPrimary());
            btn_rapor.setTextColor(fetchColorPrimary());
            btn_grup.setTextColor(fetchColorPrimary());
            btn_setting.setTextColor(fetchColorPrimary());
            setDrawablePrimary(btn_submit, R.drawable.ic_event_available_black_24dp);
            setDrawablePrimary(btn_target, R.drawable.ic_event_note_black_24dp);
            setDrawablePrimary(btn_berita, R.drawable.ic_description_black_24dp);
            setDrawablePrimary(btn_rapor, R.drawable.ic_timeline_black_24dp);
            setDrawablePrimary(btn_grup, R.drawable.ic_people_black_24dp);
            setDrawablePrimary(btn_setting, R.drawable.ic_settings_black_24dp);
        }
    }

    private void setDrawable(Button submit, int id) {
        Drawable drawable = getResources().getDrawable(id);
        drawable.setColorFilter(fetchColor(), PorterDuff.Mode.SRC_IN);
        submit.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
    }

    private void setDrawablePrimary(Button submit, int id) {
        Drawable drawable = getResources().getDrawable(id);
        drawable.setColorFilter(fetchColorPrimary(), PorterDuff.Mode.SRC_IN);
        submit.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
    }

    private void click(Class<?> activityClass) {
        Intent intent = new Intent(MainActivity.this, activityClass);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Keluar?")
                .setTitle("Konfirmasi")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        System.exit(1);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert =  builder.create();
        alert.show();
    }

    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }

    private int fetchColor() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = this.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorControlNormal });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    private int fetchColorPrimary() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = this.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimary });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }
}
