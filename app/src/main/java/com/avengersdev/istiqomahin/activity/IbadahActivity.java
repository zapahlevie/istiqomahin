package com.avengersdev.istiqomahin.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.adapter.IbadahAdapter;
import com.avengersdev.istiqomahin.controller.AppController;
import com.avengersdev.istiqomahin.helper.DBHelper;
import com.avengersdev.istiqomahin.helper.PrefManager;
import com.avengersdev.istiqomahin.model.Ibadah;
import com.avengersdev.istiqomahin.server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IbadahActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private FloatingActionButton fab;
    private ListView list;
    private SwipeRefreshLayout swipe;
    private List<Ibadah> itemList = new ArrayList<Ibadah>();
    private IbadahAdapter adapter;
    private int success;
    private AlertDialog.Builder dialog;
    private LayoutInflater inflater;
    private View dialogView;
    private EditText txt_id, txt_nama, txt_satuan, txt_target;
    private RadioGroup txt_kategori, txt_tipe;
    private String id, nama, kategori, tipe, satuan , target;
    private RadioButton radioButton1, radioButton2, radioW, radioS;
    private static final String TAG = IbadahActivity.class.getSimpleName();
    private static String url_select 	 = Server.UrlIbadah + "select.php";
    private static String url_insert 	 = Server.UrlIbadah + "insert.php";
    private static String url_edit 	     = Server.UrlIbadah + "edit.php";
    private static String url_update 	 = Server.UrlIbadah + "update.php";
    private static String url_delete 	 = Server.UrlIbadah + "delete.php";
    public static final String TAG_ID       = "id";
    public static final String TAG_NAMA     = "nama";
    public static final String TAG_KATEGORI   = "kategori";
    public static final String TAG_TIPE   = "tipe";
    public static final String TAG_SATUAN   = "satuan";
    public static final String TAG_TARGET   = "target";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private DBHelper db;
    private PrefManager prefManager;
    private String tag_json_obj = "json_obj_req";
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PrefManager prefManager = new PrefManager(getApplicationContext());
        applyTheme(prefManager.getTema());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ibadah);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ibadah);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Ibadah");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        db = new DBHelper(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        user_id = user.get("uid");
        fab     = (FloatingActionButton) findViewById(R.id.fab_add);
        swipe   = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        list    = (ListView) findViewById(R.id.list);
        adapter = new IbadahAdapter(IbadahActivity.this, itemList);
        list.setAdapter(adapter);
        swipe.setOnRefreshListener(this);
        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           itemList.clear();
                           adapter.notifyDataSetChanged();
                           callVolley();
                       }
                   }
        );
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogForm("", "", "", "", "", "", "SIMPAN");
            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {
                final String idx = itemList.get(position).getId();
                final CharSequence[] dialogitem = {"Edit", "Delete"};
                dialog = new AlertDialog.Builder(IbadahActivity.this);
                dialog.setCancelable(true);
                dialog.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        switch (which) {
                            case 0:
                                edit(idx);
                                break;
                            case 1:
                                delete(idx);
                                break;
                        }
                    }
                }).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        Drawable drawable = menu.getItem(0).getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refresh:
                swipe.setRefreshing(true);
                itemList.clear();
                adapter.notifyDataSetChanged();
                callVolley();
                return(true);
        }
        return(super.onOptionsItemSelected(item));
    }

    @Override
    public void onRefresh() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        callVolley();
    }

    private void kosong(){
        txt_id.setText(null);
        txt_nama.setText(null);
        txt_satuan.setText(null);
        txt_target.setText(null);
    }

    private void DialogForm(String idx, String namax, String kategorix, String tipex, String satuanx, String targetx, String button) {
        dialog = new AlertDialog.Builder(IbadahActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_ibadah, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        txt_id      = (EditText) dialogView.findViewById(R.id.txt_id);
        txt_nama    = (EditText) dialogView.findViewById(R.id.txt_nama);
        txt_kategori  = (RadioGroup) dialogView.findViewById(R.id.txt_kategori);
        txt_satuan  = (EditText) dialogView.findViewById(R.id.txt_satuan);
        txt_target  = (EditText) dialogView.findViewById(R.id.txt_target);
        radioButton1 = (RadioButton) dialogView.findViewById(R.id.r_check);
        radioButton2 = (RadioButton) dialogView.findViewById(R.id.r_isian);
        radioW = (RadioButton) dialogView.findViewById(R.id.r_wajib);
        radioS = (RadioButton) dialogView.findViewById(R.id.r_sunnah);
        txt_tipe  = (RadioGroup) dialogView.findViewById(R.id.radios);
        if(radioButton1.isChecked()){
            txt_satuan.setVisibility(View.GONE);
            txt_target.setVisibility(View.GONE);
        }
        else{
            txt_satuan.setVisibility(View.VISIBLE);
            txt_target.setVisibility(View.VISIBLE);
        }
        txt_tipe.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(radioButton1.isChecked()){
                    txt_satuan.setVisibility(View.GONE);
                    txt_target.setVisibility(View.GONE);
                }
                else{
                    txt_satuan.setVisibility(View.VISIBLE);
                    txt_target.setVisibility(View.VISIBLE);
                }
            }
        });
        if (!idx.isEmpty()){
            dialog.setTitle("Edit Ibadah");
            txt_id.setText(idx);
            txt_nama.setText(namax);
            if(kategorix.equals("Wajib")){
                radioW.setChecked(true);
            }
            else{
                radioS.setChecked(true);
            }
            if(tipex.equals("Checklist")){
                radioButton1.setChecked(true);
            }
            else{
                radioButton2.setChecked(true);
            }
            txt_satuan.setText(satuanx);
            txt_target.setText(targetx);
        } else {
            dialog.setTitle("Tambah Ibadah");
            kosong();
        }
        dialog.setPositiveButton(button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                id      = txt_id.getText().toString();
                nama    = txt_nama.getText().toString();
                kategori  = (String) ((RadioButton) dialogView.findViewById(txt_kategori.getCheckedRadioButtonId())).getText();
                tipe  = (String) ((RadioButton) dialogView.findViewById(txt_tipe.getCheckedRadioButtonId())).getText();
                satuan  = txt_satuan.getText().toString();
                if(tipe.equals("Checklist")){
                    satuan = "kali";
                }
                target  = txt_target.getText().toString();
                if(tipe.equals("Checklist")){
                    target = "1";
                }

                simpan_update();
                dialog.dismiss();

            }
        });
        dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                kosong();
            }
        });
        dialog.show();
    }

    private void callVolley(){
        itemList.clear();
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(true);
        JsonArrayRequest jArr = new JsonArrayRequest(url_select+"?user_id="+user_id, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        Ibadah item = new Ibadah();
                        item.setId(obj.getString(TAG_ID));
                        item.setNama(obj.getString(TAG_NAMA));
                        item.setKategori(obj.getString(TAG_KATEGORI));
                        item.setTipe(obj.getString(TAG_TIPE));
                        item.setSatuan(obj.getString(TAG_SATUAN));
                        item.setTarget(obj.getString(TAG_TARGET));
                        itemList.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private void simpan_update() {
        String url;
        if (id.isEmpty()){
            url = url_insert+"?user_id="+user_id;
        } else {
            url = url_update;
        }

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Cek error node pada json
                    if (success == 1) {
                        Log.d("Add/update", jObj.toString());
                        callVolley();
                        kosong();
                        Toast.makeText(IbadahActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(IbadahActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(IbadahActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (id.isEmpty()){
                    params.put("nama", nama);
                    params.put("kategori", kategori);
                    params.put("tipe", tipe);
                    params.put("satuan", satuan);
                    params.put("target", target);
                } else {
                    params.put("id", id);
                    params.put("nama", nama);
                    params.put("kategori", kategori);
                    params.put("tipe", tipe);
                    params.put("satuan", satuan);
                    params.put("target", target);
                }
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void edit(final String idx){
        StringRequest strReq = new StringRequest(Request.Method.POST, url_edit, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());
                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);
                    if (success == 1) {
                        Log.d("get edit data", jObj.toString());
                        String idx      = jObj.getString(TAG_ID);
                        String namax    = jObj.getString(TAG_NAMA);
                        String kategorix  = jObj.getString(TAG_KATEGORI);
                        String tipex  = jObj.getString(TAG_TIPE);
                        String satuanx  = jObj.getString(TAG_SATUAN);
                        String targetx  = jObj.getString(TAG_TARGET);
                        DialogForm(idx, namax, kategorix, tipex, satuanx, targetx, "UPDATE");
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(IbadahActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(IbadahActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idx);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void delete(final String idx){
        StringRequest strReq = new StringRequest(Request.Method.POST, url_delete, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());
                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);
                    if (success == 1) {
                        Log.d("delete", jObj.toString());
                        callVolley();
                        Toast.makeText(IbadahActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(IbadahActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(IbadahActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idx);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }
}
