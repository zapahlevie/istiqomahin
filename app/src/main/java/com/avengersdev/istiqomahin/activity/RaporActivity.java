package com.avengersdev.istiqomahin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.adapter.RaporAdapter;
import com.avengersdev.istiqomahin.controller.AppController;
import com.avengersdev.istiqomahin.helper.DBHelper;
import com.avengersdev.istiqomahin.helper.PrefManager;
import com.avengersdev.istiqomahin.model.Rapor;
import com.avengersdev.istiqomahin.model.RaporHarian;
import com.avengersdev.istiqomahin.server.Server;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RaporActivity extends AppCompatActivity {

    private ArrayList<Rapor> data = new ArrayList<Rapor>();
    private ArrayList<RaporHarian> dataHarian = new ArrayList<RaporHarian>();
    private ProgressDialog pDialog;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private DBHelper db;
    private PrefManager prefManager;
    private String user_id;
    private View dialogView;
    private AlertDialog.Builder dialog;
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefManager = new PrefManager(getApplicationContext());
        applyTheme(prefManager.getTema());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rapor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_rapor);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Rapor");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        db = new DBHelper(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        user_id = user.get("uid");
        initView();
    }

    private void initView() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RaporAdapter(RaporActivity.this, dataHarian);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child != null && gestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(child);
                    dialog = new AlertDialog.Builder(RaporActivity.this);
                    inflater = getLayoutInflater();
                    dialogView = inflater.inflate(R.layout.dialog_rapor, null);
                    dialog.setView(dialogView);
                    dialog.setCancelable(true);
                    TextView tv = dialogView.findViewById(R.id.desc);
                    String date = dataHarian.get(position).getTanggal();
                    String tanggal[] = date.toString().split("-");
                    String dataRapor = "";
                    for(int i = 0; i < data.size(); i++){
                        if(data.get(i).getTanggal().equals(date)){
                            dataRapor += data.get(i).getNamaIbadah()+"("+data.get(i).getTercapai()+"/"+data.get(i).getTarget()+")\n";
                        }
                    }
                    dataRapor = dataRapor.substring(0, dataRapor.length() - 1);
                    tv.setText(dataRapor);
                    dialog.setTitle(tanggal[2]+"/"+tanggal[1]+"/"+tanggal[0]);
                    dialog.setNegativeButton("TUTUP", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        JsonArrayRequest jArr = new JsonArrayRequest(Server.UrlRapor+"select_all.php?user_id="+user_id,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(RaporActivity.class.getSimpleName(), response.toString());
                        hidePDialog();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Rapor rapor = new Rapor();
                                rapor.setId(obj.getString("id"));
                                rapor.setNamaIbadah(obj.getString("nama_ibadah"));
                                rapor.setTercapai(obj.getString("tercapai"));
                                rapor.setTarget(obj.getString("target"));
                                rapor.setStatus(obj.getString("status"));
                                rapor.setTanggal(obj.getString("tanggal"));
                                data.add(rapor);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        String temp = "";
                        ArrayList<Rapor> arrayTemp = new ArrayList<Rapor>();
                        RaporHarian raporHarian = null;
                        float temppres = 0f, jml = 0f;
                        for (int i = 0; i < data.size(); i++) {
                            if(!temp.equals(data.get(i).getTanggal())){
                                if(i != 0){
                                    Float pres = (temppres*100)/jml;
                                    raporHarian.setPersentase(""+Math.round(pres));
                                    raporHarian.setArrayList(arrayTemp);
                                    dataHarian.add(raporHarian);
                                }
                                raporHarian = new RaporHarian();
                                raporHarian.setTanggal(data.get(i).getTanggal());
                                temp = data.get(i).getTanggal();
                                arrayTemp.clear();
                                temppres = 0f;
                                jml = 0f;
                                arrayTemp.add(data.get(i));
                                float tercapai = Float.valueOf(data.get(i).getTercapai());
                                float target = Float.valueOf(data.get(i).getTarget());
                                float cek = tercapai/target;
                                if(cek > 1)
                                    cek = 1;
                                temppres = temppres + cek;
                                jml++;
                            }
                            else{
                                arrayTemp.add(data.get(i));
                                float tercapai = Float.valueOf(data.get(i).getTercapai());
                                float target = Float.valueOf(data.get(i).getTarget());
                                float cek = tercapai/target;
                                if(cek > 1)
                                    cek = 1;
                                temppres = temppres + cek;
                                jml++;
                            }
                            if(i == (data.size()-1)){
                                Float pres = (temppres*100)/jml;
                                raporHarian.setPersentase(""+Math.round(pres));
                                raporHarian.setArrayList(arrayTemp);
                                dataHarian.add(raporHarian);
                            }
                        }
                        adapter.notifyDataSetChanged();
                        ArrayList<BarEntry> entries = new ArrayList<>();
                        ArrayList<String> labels = new ArrayList<String>();
                        for(int i = dataHarian.size() - 1; i >= 0 ; i--){
                            Float persentase = Float.valueOf(dataHarian.get(i).getPersentase());
                            String[] tgl = dataHarian.get(i).getTanggal().split("-");
                            entries.add(new BarEntry((dataHarian.size()-1)-i, Math.round(persentase), tgl[2]+"/"+tgl[1]));
                            labels.add(tgl[2]+"/"+tgl[1]);
                        }
                        BarChart chart = (BarChart)findViewById(R.id.bar_chart);
                        BarDataSet dataset = new BarDataSet(entries, "Tercapai");
                        dataset.setColor(fetchColorAccent());
                        BarData barData = new BarData(dataset);
                        chart.setDescription(null);
                        chart.setData(barData);
                        chart.setDoubleTapToZoomEnabled(false);
                        chart.setPinchZoom(false);
                        chart.setScaleEnabled(false);
                        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
                        chart.animateY(2000);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(RaporActivity.class.getSimpleName(), "Error: " + error.getMessage());
                hidePDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private int fetchColorAccent() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorAccent });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }
}
