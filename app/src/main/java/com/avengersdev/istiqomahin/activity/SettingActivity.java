package com.avengersdev.istiqomahin.activity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.avengersdev.istiqomahin.NotificationPublisher;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.helper.DBHelper;
import com.avengersdev.istiqomahin.helper.PrefManager;

import java.util.Calendar;

public class SettingActivity extends PreferenceActivity {

    private AppCompatDelegate mDelegate;
    private PrefManager prefManager;
    private DBHelper db;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefManager = new PrefManager(getApplicationContext());
        db = new DBHelper(getApplicationContext());
        applyTheme(prefManager.getTema());
        getDelegate().installViewFactory();
        getDelegate().onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_setting);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addPreferencesFromResource(R.xml.user_settings);

        Preference kompas = (Preference) findPreference("kompas");
        Preference kalender = (Preference) findPreference("kalender");
        Preference profil= (Preference) findPreference("profil");
        Preference tema = (Preference) findPreference("tema");
        Preference notif = (Preference) findPreference("notif");
        Preference web = (Preference) findPreference("web");
        Preference logout = (Preference) findPreference("logout");

        kompas.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingActivity.this, KiblatActivity.class));
                return false;
            }
        });
        kalender.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingActivity.this, KalenderActivity.class));
                return false;
            }
        });
        profil.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingActivity.this, ProfilActivity.class));
                return false;
            }
        });
        tema.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingActivity.this, TemaActivity.class));
                return false;
            }
        });
        notif.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingActivity.this, NotifActivity.class));
                return false;
            }
        });
        web.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(SettingActivity.this, WebActivity.class));
                return false;
            }
        });
        logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                prefManager.setLogin(false);
                db.deleteUsers();
                Intent intent = new Intent(SettingActivity.this, LoginActivity.class);;;
                startActivity(intent);
                finishAffinity();
                return false;
            }
        });

        final CheckBoxPreference checkboxPref = (CheckBoxPreference) getPreferenceManager().findPreference("dailynotif");

        checkboxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if(newValue.toString().equals("true")){
                    Notification.Builder builder = new Notification.Builder(getApplicationContext());
                    builder.setContentTitle("Selamat Malam!");
                    builder.setContentText("Isi capaian ibadahnya ya:)");
                    builder.setSmallIcon(R.drawable.ic_launcher_background);
                    Intent notificationIntent = new Intent(getApplicationContext(), NotificationPublisher.class);
                    notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
                    notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, builder.build());
                    pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(System.currentTimeMillis());
                    calendar.set(Calendar.HOUR_OF_DAY, 22);
                    alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY , pendingIntent);
                    //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);
                    Toast.makeText(SettingActivity.this, "Notifikasi aktif", Toast.LENGTH_SHORT).show();
                }
                else{
                    try{
                        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
                        manager.cancel(pendingIntent);
                    }catch(NullPointerException e){

                    }
                    Toast.makeText(SettingActivity.this, "Notifikasi nonaktif", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getDelegate().onPostCreate(savedInstanceState);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        getDelegate().setContentView(layoutResID);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        getDelegate().onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getDelegate().onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDelegate().onDestroy();
    }

    private void setSupportActionBar(@Nullable Toolbar toolbar) {
        getDelegate().setSupportActionBar(toolbar);
    }

    private AppCompatDelegate getDelegate() {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(this, null);
        }
        return mDelegate;
    }

    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }
}
