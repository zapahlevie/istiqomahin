package com.avengersdev.istiqomahin.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.adapter.GrupAdapter;
import com.avengersdev.istiqomahin.controller.AppController;
import com.avengersdev.istiqomahin.helper.DBHelper;
import com.avengersdev.istiqomahin.helper.PrefManager;
import com.avengersdev.istiqomahin.server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GrupActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private PrefManager prefManager;
    private DBHelper db;
    private String user_id, grup_id, name, mail;
    private HashMap<String, String> hmap = new HashMap<String, String>();
    private TextView nama_grup, jumlah_member, email_grup, code_grup, leave_grup;
    private RequestQueue rq;
    private Button join_grup;
    private EditText join_code;
    private CardView cardView, cardJoin;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<String> data = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefManager = new PrefManager(getApplicationContext());
        applyTheme(prefManager.getTema());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_grup);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Grup");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        db = new DBHelper(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        user_id = user.get("uid");
        name = user.get("name");
        mail = user.get("email");
        grup_id = user.get("grup_id");

        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new GrupAdapter(GrupActivity.this, data);
        recyclerView.setAdapter(adapter);
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        rq = Volley.newRequestQueue(getApplicationContext());
        cardView = (CardView)findViewById(R.id.card_grup);
        cardJoin = (CardView)findViewById(R.id.card_join);
        nama_grup = (TextView)findViewById(R.id.nama_grup);
        jumlah_member = (TextView)findViewById(R.id.jumlah_member);
        email_grup = (TextView)findViewById(R.id.email_grup);
        code_grup = (TextView)findViewById(R.id.code_grup);
        join_code = (EditText)findViewById(R.id.join_code);
        join_grup = (Button)findViewById(R.id.join_grup);
        join_grup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinGrup();
            }
        });
        leave_grup = (TextView)findViewById(R.id.leave);
        leave_grup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(GrupActivity.this);
                builder.setMessage("Yakin ingin meninggalkan grup?")
                        .setTitle("Konfirmasi")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                leaveGrup();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert =  builder.create();
                alert.show();
            }
        });
        if(Integer.valueOf(grup_id) > 0){
            cardView.setVisibility(View.VISIBLE);
            cardJoin.setVisibility(View.GONE);
            ViewGrupInfo();
            viewGrupMember();
        }
        else{
            cardView.setVisibility(View.GONE);
            cardJoin.setVisibility(View.VISIBLE);
        }
    }

    private void viewGrupMember() {
        JsonArrayRequest jArr = new JsonArrayRequest(Server.UrlUser+"member_grup.php?grup_id="+grup_id,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(GrupActivity.class.getSimpleName(), response.toString());
                        hidePDialog();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                data.add(obj.getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                        jumlah_member.setText(response.length()+" Anggota");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(GrupActivity.class.getSimpleName(), "Error: " + error.getMessage());
                hidePDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private void ViewGrupInfo() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();;
        JsonArrayRequest jArr = new JsonArrayRequest(Server.UrlUser+"select_grup.php?grup_id="+grup_id,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(GrupActivity.class.getSimpleName(), response.toString());
                        hidePDialog();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                hmap = new HashMap<String, String>();
                                hmap.put("nama_grup", obj.getString("nama_grup"));
                                hmap.put("email_grup", obj.getString("email_grup"));
                                hmap.put("code_grup", obj.getString("code_grup"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        nama_grup.setText(hmap.get("nama_grup"));
                        email_grup.setText(hmap.get("email_grup"));
                        code_grup.setText("Code : "+hmap.get("code_grup"));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hidePDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private void joinGrup() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();;
        String code = join_code.getText().toString();
        JsonArrayRequest jArr = new JsonArrayRequest(Server.UrlUser+"join_grup.php?id="+user_id+"&code_grup="+code,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(GrupActivity.class.getSimpleName(), response.toString());
                        hidePDialog();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                hmap = new HashMap<String, String>();
                                hmap.put("grup_id", obj.getString("id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Toast.makeText(GrupActivity.this, "Berhasil bergabung", Toast.LENGTH_SHORT).show();
                        grup_id = hmap.get("grup_id");
                        db.updateUser(name, mail, grup_id);
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GrupActivity.this, "Grup tidak ditemukan", Toast.LENGTH_SHORT).show();
                hidePDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jArr);
    }

    private void leaveGrup() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();;
        String url = Server.UrlUser+"leave_grup.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        hidePDialog();
                        Log.d("Response", response);
                        if(response.contains("berhasil")){
                            Toast.makeText(GrupActivity.this, "Berhasil keluar grup", Toast.LENGTH_SHORT).show();
                            grup_id = "0";
                            db.updateUser(name, mail, grup_id);
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                        else{
                            Toast.makeText(GrupActivity.this, response, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast.makeText(GrupActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id", user_id);
                return params;
            }
        };
        rq.add(postRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void applyTheme(String theme){
        switch (theme){
            case "theme1":
                setTheme(R.style.AppTheme);
                break;
            case "theme2":
                setTheme(R.style.AppTheme_theme2);
                break;
            case "theme3":
                setTheme(R.style.AppTheme_theme3);
                break;
            case "theme4":
                setTheme(R.style.AppTheme_theme4);
                break;
            case "theme5":
                setTheme(R.style.AppTheme_theme5);
                break;
            case "theme6":
                setTheme(R.style.AppTheme_theme6);
                break;
            case "theme7":
                setTheme(R.style.AppTheme_theme7);
                break;
            case "theme8":
                setTheme(R.style.AppTheme_theme8);
                break;
            default:
                break;
        }
    }
}
