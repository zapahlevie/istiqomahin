package com.avengersdev.istiqomahin.server;

/**
 * Created by zap on 12/11/2017.
 */

public class Server {
    public static final String UrlLogin = "http://istiqomah.in/api/api_login/login.php";
    public static final String UrlRegister = "http://istiqomah.in/api/api_login/register.php";
    public static final String UrlBerita = "http://istiqomah.in/api/api_berita/";
    public static final String UrlFotoBerita = "http://istiqomah.in/api/api_berita/foto_berita/";
    public static final String UrlIbadah = "http://istiqomah.in/api/api_ibadah/";
    public static final String UrlRapor = "http://istiqomah.in/api/api_rapor/";
    public static final String UrlUser = "http://istiqomah.in/api/api_user/";
    public static final String UrlPhoto = "http://istiqomah.in/api/api_user/photo/";
}
