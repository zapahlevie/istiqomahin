package com.avengersdev.istiqomahin.model;

/**
 * Created by zap on 12/11/2017.
 */

public class Rapor {
    private String id, namaIbadah, tercapai, target, kategori, status, tanggal, tipe, satuan;
    private boolean isSelected;
    public Rapor() {
    }

    public Rapor(String id, String namaIbadah, String tercapai, String target, String kategori, String status, String tanggal, String tipe, String gambar, String satuan) {
        this.id = id;
        this.namaIbadah = namaIbadah;
        this.tercapai = tercapai;
        this.target = target;
        this.kategori = kategori;
        this.status = status;
        this.tanggal = tanggal;
        this.tipe = tipe;
        this.satuan = satuan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaIbadah() {
        return namaIbadah;
    }

    public void setNamaIbadah(String namaIbadah) {
        this.namaIbadah = namaIbadah;
    }

    public String getTercapai() {
        return tercapai;
    }

    public void setTercapai(String tercapai) {
        this.tercapai = tercapai;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
