package com.avengersdev.istiqomahin.model;

/**
 * Created by zap on 12/10/2017.
 */

public class Berita {
    String id;
    String judul;
    String gambar;

    public Berita() {

    }

    public Berita(String id, String judul, String gambar) {
        this.id = id;
        this.judul = judul;
        this.gambar = gambar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
