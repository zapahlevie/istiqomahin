package com.avengersdev.istiqomahin.model;

import java.util.ArrayList;

/**
 * Created by zap on 12/13/2017.
 */

public class RaporHarian {
    private String tanggal, persentase;
    private ArrayList<Rapor> arrayList = new ArrayList<Rapor>();

    public RaporHarian() {
    }

    public RaporHarian(String tanggal, String persentase, ArrayList<Rapor> arrayList) {
        this.tanggal = tanggal;
        this.persentase = persentase;
        this.arrayList = arrayList;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getPersentase() {
        return persentase;
    }

    public void setPersentase(String persentase) {
        this.persentase = persentase;
    }

    public ArrayList<Rapor> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Rapor> arrayList) {
        this.arrayList = arrayList;
    }
}
