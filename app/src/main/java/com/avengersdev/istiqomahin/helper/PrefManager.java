package com.avengersdev.istiqomahin.helper;

/**
 * Created by zap on 10/8/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "local";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_LOGGEDIN = "isLoggedIn";
    private static final String IS_SUBMITTED = "isSubmitted";
    private static final String IS_JOINED = "isJoined";
    private static final String THEME = "theme";
    private static final String TIME_SUBH = "subh";
    private static final String TIME_DUHR = "duhr";
    private static final String TIME_ASHR = "ashr";
    private static final String TIME_MGRB = "mgrb";
    private static final String TIME_ISHA = "isha";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(IS_LOGGEDIN, isLoggedIn);
        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGGEDIN, false);
    }

    public void setSubmitted(String isSubmitted) {
        editor.putString(IS_SUBMITTED, isSubmitted);
        editor.commit();
    }

    public boolean isSubmitted(String date){
        if(pref.getString(IS_SUBMITTED, "").equals(date)){
            return true;
        }
        else{
            return false;
        }
    }

    public void setTema(String tema) {
        editor.putString(THEME, tema);
        editor.commit();
    }

    public String getTema(){
        return pref.getString(THEME, "theme1");
    }

    public void setSubh(String subh) {
        editor.putString(TIME_SUBH, subh);
        editor.commit();
    }

    public String getSubh(){
        return pref.getString(TIME_SUBH, "04:00");
    }

    public void setDuhr(String duhr) {
        editor.putString(TIME_DUHR, duhr);
        editor.commit();
    }

    public String getDuhr(){
        return pref.getString(TIME_DUHR, "12:00");
    }

    public void setAshr(String ashr) {
        editor.putString(TIME_ASHR, ashr);
        editor.commit();
    }

    public String getAshr(){
        return pref.getString(TIME_ASHR, "15:00");
    }

    public void setMgrb(String mgrb) {
        editor.putString(TIME_MGRB, mgrb);
        editor.commit();
    }

    public String getMgrb(){
        return pref.getString(TIME_MGRB, "18:00");
    }

    public void setIsha(String isha) {
        editor.putString(TIME_ISHA, isha);
        editor.commit();
    }

    public String getIsha(){
        return pref.getString(TIME_ISHA, "19:00");
    }

}
