package com.avengersdev.istiqomahin.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.model.Rapor;

import java.util.ArrayList;

public class SubmitAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Rapor> modelArrayList;

    public SubmitAdapter(Context context, ArrayList<Rapor> modelArrayList) {
        this.context = context;
        this.modelArrayList = modelArrayList;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.card_row_submit, null, true);
            holder.nama = (TextView) convertView.findViewById(R.id.nama);
            holder.tercapai_isi = (EditText) convertView.findViewById(R.id.tercapai_isi);
            holder.tercapai_cek = (CheckBox) convertView.findViewById(R.id.tercapai_cek);
            holder.target = (TextView) convertView.findViewById(R.id.target);
            holder.kategori = (TextView) convertView.findViewById(R.id.kategori);
            holder.detail = (TextView) convertView.findViewById(R.id.detail);
            holder.view_submit = (View) convertView.findViewById(R.id.view_submit);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.nama.setText(modelArrayList.get(position).getNamaIbadah());
        holder.target.setText(modelArrayList.get(position).getTarget());
        holder.kategori.setText(modelArrayList.get(position).getKategori());
        holder.detail.setText("Target : "+modelArrayList.get(position).getTarget()+" "+modelArrayList.get(position).getSatuan());
        if(modelArrayList.get(position).getKategori().equals("Wajib")){
            holder.view_submit.setBackgroundColor(fetchColor());
        }
        else{
            holder.view_submit.setBackgroundColor(fetchColorAccent());
        }
        if(modelArrayList.get(position).getTipe().equals("Isian")){
            holder.tercapai_isi.setVisibility(View.VISIBLE);
            holder.tercapai_cek.setVisibility(View.GONE);
        }
        else{
            holder.tercapai_isi.setVisibility(View.GONE);
            holder.tercapai_cek.setVisibility(View.VISIBLE);
        }
        holder.tercapai_isi.setText(modelArrayList.get(position).getTercapai());
        holder.tercapai_isi.setTag(position);
        holder.tercapai_isi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    final int position = (Integer)view.getTag();
                    final EditText Caption = (EditText)view;
                    modelArrayList.get(position).setTercapai(Caption.getText().toString());
                }
            }
        });
        holder.tercapai_cek.setChecked(modelArrayList.get(position).isSelected());
        holder.tercapai_cek.setTag(R.integer.btnplusview, convertView);
        holder.tercapai_cek.setTag(position);
        holder.tercapai_cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer pos = (Integer)holder.tercapai_cek.getTag();
                if (modelArrayList.get(pos).isSelected()){
                    modelArrayList.get(pos).setSelected(false);
                }
                else {
                    modelArrayList.get(pos).setSelected(true);
                }

            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView nama;
        EditText tercapai_isi;
        CheckBox tercapai_cek;
        TextView target;
        TextView kategori;
        TextView detail;
        View view_submit;
    }

    private int fetchColor() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimary });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    private int fetchColorAccent() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorAccent });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }
}