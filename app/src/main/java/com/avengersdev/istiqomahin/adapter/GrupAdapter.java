package com.avengersdev.istiqomahin.adapter;

/**
 * Created by zap on 12/7/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avengersdev.istiqomahin.R;

import java.util.ArrayList;

public class GrupAdapter extends RecyclerView.Adapter<GrupAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<String> arrayList = new ArrayList<String>();

    public GrupAdapter(Activity activity, ArrayList<String> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public GrupAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row_grup, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GrupAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.member.setText(arrayList.get(i));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView member;
        public ViewHolder(View view) {
            super(view);
            member = (TextView)view.findViewById(R.id.member);
        }
    }

}