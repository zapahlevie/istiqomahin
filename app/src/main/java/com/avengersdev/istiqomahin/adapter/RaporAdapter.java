package com.avengersdev.istiqomahin.adapter;

/**
 * Created by zap on 12/7/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.model.RaporHarian;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RaporAdapter extends RecyclerView.Adapter<RaporAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<RaporHarian> arrayList = new ArrayList<RaporHarian>();

    public RaporAdapter(Activity activity, ArrayList<RaporHarian> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public RaporAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row_rapor, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RaporAdapter.ViewHolder viewHolder, final int i) {
        String full_date = arrayList.get(i).getTanggal();
        String persentase = arrayList.get(i).getPersentase();
        String[] splits = full_date.split("-");
        String tahun = splits[0];
        String bulan = toDate(splits[1]);
        String tanggal = splits[2];
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-M-d").parse(full_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dayOfWeek = new SimpleDateFormat("E", Locale.ENGLISH).format(date);
        viewHolder.hari.setText(toDay(dayOfWeek));
        viewHolder.full_date.setText(full_date);
        viewHolder.tanggal.setText(tanggal);
        viewHolder.bulan_tahun.setText(bulan+" "+tahun);
        viewHolder.persentase.setText(persentase+"%");
        float width = Float.valueOf(persentase)+150f;
        ViewGroup.LayoutParams params=viewHolder.view_persentase.getLayoutParams();
        params.width=(int)width;
        viewHolder.view_persentase.setLayoutParams(params);
    }

    private String toDate(String split) {
        String bulan = "";
        switch (split){
            case "01":
                bulan = "Januari";
                break;
            case "02":
                bulan = "Februari";
                break;
            case "03":
                bulan = "Maret";
                break;
            case "04":
                bulan = "April";
                break;
            case "05":
                bulan = "Mei";
                break;
            case "06":
                bulan = "Juni";
                break;
            case "07":
                bulan = "Juli";
                break;
            case "08":
                bulan = "Agustus";
                break;
            case "09":
                bulan = "September";
                break;
            case "10":
                bulan = "Oktober";
                break;
            case "11":
                bulan = "November";
                break;
            case "12":
                bulan = "Desember";
                break;
            default:
                break;
        }
        return bulan;
    }

    private String toDay(String split) {
        String bulan = "";
        switch (split){
            case "Mon":
                bulan = "Senin";
                break;
            case "Tue":
                bulan = "Selasa";
                break;
            case "Wed":
                bulan = "Rabu";
                break;
            case "Thu":
                bulan = "Kamis";
                break;
            case "Fri":
                bulan = "Jumat";
                break;
            case "Sat":
                bulan = "Sabtu";
                break;
            case "Sun":
                bulan = "Minggu";
                break;
            default:
                break;
        }
        return bulan;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView full_date;
        private TextView hari;
        private TextView tanggal;
        private TextView bulan_tahun;
        private TextView persentase;
        private View view_persentase;
        public ViewHolder(View view) {
            super(view);
            full_date = (TextView)view.findViewById(R.id.full_date);
            hari = (TextView)view.findViewById(R.id.hari);
            tanggal = (TextView)view.findViewById(R.id.tanggal);
            bulan_tahun = (TextView)view.findViewById(R.id.bulan_tahun);
            persentase = (TextView)view.findViewById(R.id.persentase);
            view_persentase = (View)view.findViewById(R.id.view_persentase);
        }
    }



}