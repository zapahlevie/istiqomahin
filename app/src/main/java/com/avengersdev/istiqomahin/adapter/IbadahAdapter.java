package com.avengersdev.istiqomahin.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.model.Ibadah;

import java.util.List;

public class IbadahAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Ibadah> items;

    public IbadahAdapter(Activity activity, List<Ibadah> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int location) {
        return items.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.card_row_ibadah, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView nama = (TextView) convertView.findViewById(R.id.nama);
        TextView kategori = (TextView) convertView.findViewById(R.id.kategori);
        View view = (View) convertView.findViewById(R.id.view_ibadah);

        Ibadah ibadah = items.get(position);
        if(ibadah.getKategori().equals("Wajib")){
            view.setBackgroundColor(fetchColor());
        }
        else{
            view.setBackgroundColor(fetchColorAccent());
        }
        id.setText(ibadah.getId());
        nama.setText(ibadah.getNama());
        kategori.setText(ibadah.getKategori()+": "+ibadah.getTarget()+" "+ibadah.getSatuan());

        return convertView;
    }

    private int fetchColor() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = activity.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimary });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    private int fetchColorAccent() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = activity.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorAccent });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

}