package com.avengersdev.istiqomahin.adapter;

/**
 * Created by zap on 12/7/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avengersdev.istiqomahin.R;
import com.avengersdev.istiqomahin.server.Server;
import com.avengersdev.istiqomahin.model.Berita;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<Berita> arrayList = new ArrayList<Berita>();

    public BeritaAdapter(Activity activity, ArrayList<Berita> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public BeritaAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row_berita, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BeritaAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.judul.setText(arrayList.get(i).getJudul());
        Picasso.with(activity.getApplicationContext())
                .load(Server.UrlFotoBerita+arrayList.get(i).getGambar())
                .error(R.mipmap.ic_launcher)
                .into(viewHolder.gambar);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView judul;
        private ImageView gambar;
        public ViewHolder(View view) {
            super(view);
            judul = (TextView)view.findViewById(R.id.judul);
            gambar = (ImageView)view.findViewById(R.id.gambar);
        }
    }

}